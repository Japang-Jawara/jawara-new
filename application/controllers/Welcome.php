<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() {
        parent::__construct();
        $this->load->model('Home_model', 'modelHome');
    }

	public function index()
	{
		$x['totalLeads'] = $this->modelHome->totalLeads();
		$x['leadsHariIni'] = $this->modelHome->leadsHariIni();
		$x['notyetKYC'] = $this->modelHome->notyetKYC();
		$x['doneKYC'] = $this->modelHome->doneKYC();
		$x['doneKYCHari'] = $this->modelHome->doneKYCHari();

		$x['lolosSurvey'] = $this->modelHome->lolosSurvey();
		$x['lolosSurveyHari'] = $this->modelHome->lolosSurveyHari();
		$x['totalDisbursement'] = $this->modelHome->totalDisbursement();
		$x['totalDisbursementHari'] = $this->modelHome->totalDisbursementHari();

		$x['totalDeploy'] = $this->modelHome->totalDeploy();
		$x['totalDeployHari'] = $this->modelHome->totalDeployHari();

		$x['totalperProvinsi'] = $this->modelHome->totalperProvinsi();

		$chart = $this->modelHome->getTotal()->result();

		$chart = json_encode($chart);

		$kat = "";
		$arr = $tmp = array();
		foreach (json_decode($chart, true) as $z):
			array_push($tmp, array(
				'total'			=>  $z['total'],
				'tanggal'		=>	$z['tanggal']
			));

		endforeach;
		array_push($arr, $tmp);

		$x['chart'] = $arr;

		$pendaftar = $this->modelHome->getTotal()->result();
		$x['pendaftar'] = $pendaftar;

		$x['maps'] = $this->modelHome->maps();

		// $this->load->view("include/head");
		// $this->load->view("include/top-header");
		$this->load->view('home.php', $x);
		// $this->load->view("include/sidebar");
		// $this->load->view("include/panel");
		// $this->load->view("include/footer");
		// $this->load->view("include/alert");
	}
}
