<?php
class Score_model extends CI_Model{

    public function getScoreTinggi(){
        return $this->db->select('*')->from('jawara_score')->where('status_score_sc', 1)->get()->result();
    }

    public function getScoreRendah(){
        return $this->db->select('*')->from('jawara_score')->where('status_score_sc', 0)->get()->result();
    }
}
?>