<?php
class Profile_model extends CI_Model{

    public function getJawaraId($id){
        return $this->db->select('*')->from('jawara_leads')->where('id', $id)->get()->result();
    }

    public function getJawaraKyc($id){
        return $this->db->select('*')->from('jawara_leads')->where('id', $id)->get()->result();
    }

    public function getJawaraDigital($id){
        return $this->db->select('*')->from('jawara_leads')->where('id', $id)->get()->result();
    }

    public function getJawaraScore($id){
        return $this->db->select('*')->from('jawara_leads')->where('id', $id)->get()->result();
    }

    public function getJawaraEligible($id){
        return $this->db->select('*')->from('jawara_leads')->where('id', $id)->get()->result();
    }

    public function getJawaraDisbursementId($id){
        return $this->db->select('*')->from('jawara_leads')->where('id', $id)->get()->result();
    }

    public function getJawaraDeploy($id){
        return $this->db->select('*')->from('jawara_leads')->where('id', $id)->get()->result();
    }

    public function getProfile($id){
        return $this->db->select('*')->from('jawara_leads')->where('id', $id)->get()->result();
    }

}
?>