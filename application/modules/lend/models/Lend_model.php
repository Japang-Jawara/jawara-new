<?php
class Lend_model extends CI_Model{

    public function getLend(){
        return $this->db->select('*')->from('lend_stats')->where('is_active', 1)->get()->result();
    }
    
    public function getAlldata(){
        return $this->db->query("SELECT
            *
            FROM jawara_leads
            WHERE (status_jawara_di = 1  OR status_score_sc = 1) AND status_eligible IS NULL")
        ->result();
    }

    public function getP2p(){
        return $this->db->select('*')->from('jawara_leads')->where('status_eligible', 1)->get()->result();
    }

    public function eligible($jawara_id, $add){

        
        $this->db->where('id', $jawara_id);
        $this->db->update('jawara_leads', $add);

    }
}
?>