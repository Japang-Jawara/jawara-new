<div id="content" class="content">
	<ol class="breadcrumb float-xl-right">
		<li class="breadcrumb-item"><a href="javascript:;">Dashboard</a></li>
		<li class="breadcrumb-item active">Jawara</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">PT Jaring Pangan <small>Indonesia</small></h1>
	<div class="d-sm-flex align-items-center mb-3">
		<a href="#" class="btn btn-inverse mr-2 text-truncate" id="daterange-filter">
			<i class="fa fa-calendar fa-fw text-white-transparent-5 ml-n1"></i> 
			<span><?php echo date("d/m/Y"); ?></span>
		</a>
		<a href="<?= base_url('index.php/home/jawaraTV') ?>" class="btn btn-danger mr-2 text-truncate" id="daterange-filter">
			<i class="fa fa-television  fa-fw text-white-transparent-5 ml-n1"></i> 
			<span>JAWARA SMART TV</span>
		</a>
	</div>

	<div class="row">
		<!-- begin col-3 -->
		<div class="col-xl col-md-6">
			<div class="widget widget-stats bg-teal">
				<div class="stats-icon stats-icon-lg"><i class="fa fa-globe fa-fw"></i></div>
				<div class="stats-content">
					<div class="stats-title">Total Jawara Leads</div>
					<div class="stats-number"><?= $totalLeads[0]->totalLeads; ?></div>
					<div class="stats-progress progress">
						<div class="progress-bar" style="width: 100%;"></div>
					</div>
					<div class="stats-desc">+ <?= $leadsHariIni[0]->leadsHariIni; ?> Hari Ini</div>
				</div>
				<div class="stats-link">
					<a href="<?= base_url('index.php/jawara'); ?>">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
				</div>
			</div>
		</div>
		<!-- end col-3 -->
		<!-- begin col-3 -->
		<div class="col-xl col-md-6">
			<div class="widget widget-stats bg-blue">
				<div class="stats-icon stats-icon-lg"><i class="fa fa-dollar-sign fa-fw"></i></div>
				<div class="stats-content">
					<div class="stats-title">KYC Done</div>
					<div class="stats-number"><?= $doneKYC[0]->doneKYC; ?></div>
					<div class="stats-progress progress">
						<div class="progress-bar" style="width: 100%;"></div>
					</div>
					<div class="stats-desc">+ <?= $doneKYCHari[0]->doneKYCHari; ?> Hari Ini</div>
				</div>
				<div class="stats-link">
					<a href="<?= base_url('index.php/kyc'); ?>">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
				</div>
			</div>
		</div>
		<!-- end col-3 -->
		<!-- begin col-3 -->
		<div class="col-xl col-md-6">
			<div class="widget widget-stats bg-indigo">
				<div class="stats-icon stats-icon-lg"><i class="fa fa-archive fa-fw"></i></div>
				<div class="stats-content">
					<div class="stats-title">Score A-B</div>
					<div class="stats-number"><?= $lolosSurvey ?></div>
					<div class="stats-progress progress">
						<div class="progress-bar" style="width: 100%;"></div>
					</div>
					<div class="stats-desc">+ <?= $lolosSurveyHari ?> Hari Ini</div>
				</div>
				<div class="stats-link">
					<a href="<?= base_url('index.php/lend'); ?>">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
				</div>
			</div>
		</div>
		<!-- end col-3 -->
		<!-- begin col-3 -->
		<div class="col-xl col-md-6">
			<div class="widget widget-stats bg-dark">
				<div class="stats-icon stats-icon-lg"><i class="fa fa-comment-alt fa-fw"></i></div>
				<div class="stats-content">
					<div class="stats-title">Disbursements</div>
					<div class="stats-number"><?= $totalDisbursement ?></div>
					<div class="stats-progress progress">
						<div class="progress-bar" style="width: 100%;"></div>
					</div>
					<div class="stats-desc">+ <?= $totalDisbursementHari ?> Hari Ini</div>
				</div>
				<div class="stats-link">
					<a href="<?= base_url('index.php/disbursement/confirm'); ?>">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
				</div>
			</div>
		</div>
		<!-- end col-3 -->
		<div class="col-xl col-md-6">
			<div class="widget widget-stats bg-red">
				<div class="stats-icon stats-icon-lg"><i class="fa fa-truck fa-fw"></i></div>
				<div class="stats-content">
					<div class="stats-title">Deploy</div>
					<div class="stats-number"><?= $totalDeploy ?></div>
					<div class="stats-progress progress">
						<div class="progress-bar" style="width: 100%;"></div>
					</div>
					<div class="stats-desc">+ <?= $totalDeployHari ?> Hari Ini</div>
				</div>
				<div class="stats-link">
					<a href="<?= base_url('index.php/deploy'); ?>">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<!-- begin col-8 -->
		<div class="col-xl-6">
			<div class="widget-chart with-sidebar inverse-mode">
				<div class="widget-chart-content bg-dark">
					<h4 class="chart-title">
						Leads Analytics
						<small>Pendaftar Calon Jawara</small>
					</h4>
					<canvas id="myCharts1" class="d-sm-none"></canvas>
					<canvas id="myChart1" class="d-sm-block d-none"></canvas>
					<!-- <div id="visitors-line-chart" class="widget-chart-full-width nvd3-inverse-mode" style="height: 260px;"></div> -->
				</div>
				<!-- <div class="widget-chart-sidebar bg-dark-darker">
					<div class="chart-number">
						<?=  $totalLeads[0]->totalLeads ?>
						<small>Total Leads</small>
					</div>
					<div class="flex-grow-1 d-flex align-items-center">
						<div id="visitors-donut-chart" class="nvd3-inverse-mode" style="height: 180px"></div>
					</div>
					<ul class="chart-legend f-s-11">
						<li><i class="fa fa-circle fa-fw text-blue f-s-9 m-r-5 t-minus-1"></i> <?= $leadsHariIni[0]->leadsHariIni; ?> <span>New Pendaftar Calon Jawara</span></li>
						<li><i class="fa fa-circle fa-fw text-teal f-s-9 m-r-5 t-minus-1"></i> <?=  $totalLeads[0]->totalLeads ?> <span>Jumlah Calon Jawara</span></li>
					</ul>
				</div> -->
			</div>
		</div>
		<!-- end col-8 -->
		<!-- begin col-4 -->
		<div class="col-xl-6">
			<div class="panel panel-inverse" data-sortable-id="index-1">
				<!-- <div class="panel-heading">
					<h4 class="panel-title">
						
					</h4>
					<span>
						<a href="<?= base_url('index.php/home/maps') ?>" class="btn btn-sm btn-primary">Detail</a>
					<span>
				</div> -->
				
				<a href="<?= base_url('index.php/home/maps') ?>"><div id="maps-dashboard" class="bg-dark-darker" style="height: 230px;"></div></a>
				<div class="list-group">
					<?php $no = 0; foreach($totalperProvinsi as $r){ $no++?>
						<a href="javascript:;" class="list-group-item list-group-item-action list-group-item-inverse d-flex justify-content-between align-items-center text-ellipsis">
							<?= $no.'. '.$r->provinsi ?> 
							<span class="badge bg-blue f-s-10"><?= $r->total ?></span>
						</a>
					<?php } ?>
				</div>
			</div>
		</div>
		<!-- end col-4 -->
	</div>	
</div>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKpPwIGS_dVL5X8jDKY7FcVuNxK8Rg5k0&callback=initMap&v=weekly" async></script>
<script>

	let map;

	function initMap() {
        map = new google.maps.Map(document.getElementById("maps-dashboard"), {
            zoom: 5,
            center: { lat:  -7.285695813685049, lng: 109.54936137079416 },
        });

        setMarkers(map);
    }

	const beaches = [
        <?php 
        foreach($maps as $r){
            $data = explode(',',$r->gmaps_di); 
        ?>
            ["Jawara", <?= $data[0] ?>, <?= $data[1] ?>],
        <?php 
        } 
        ?>
    ];

    function setMarkers(map) {
        // Adds markers to the map.
        // Marker sizes are expressed as a Size of X,Y where the origin of the image
        // (0,0) is located in the top left of the image.
        // Origins, anchor positions and coordinates of the marker increase in the X
        // direction to the right and in the Y direction down.
        const image = {
            // url: "https://developers.google.com/maps/documentation/javascript/examples/full/images/pin.png",
            // This marker is 20 pixels wide by 32 pixels high.
            size: new google.maps.Size(20, 32),
            // The origin for this image is (0, 0).
            origin: new google.maps.Point(0, 0),
            // The anchor for this image is the base of the flagpole at (0, 32).
            anchor: new google.maps.Point(0, 32),
        };
        // Shapes define the clickable region of the icon. The type defines an HTML
        // <area> element 'poly' which traces out a polygon as a series of X,Y points.
        // The final coordinate closes the poly by connecting to the first coordinate.
        const shape = {
            coords: [1, 1, 1, 20, 18, 20, 18, 1],
            type: "poly",
        };

        for (let i = 0; i < beaches.length; i++) {
            const beach = beaches[i];

            new google.maps.Marker({
                position: { lat: beach[1], lng: beach[2] },
                map,
                icon: image,
                shape: shape,
                title: beach[0],
                zIndex: beach[3],
            });
        }
    }
</script>

<script src="<?php echo base_url().'assets/js/jquery.min.js'?>"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.0/chart.min.js"></script>

<script src="https://www.chartjs.org/dist/2.9.4/Chart.min.js"></script>

<script>
	var back = ["153, 102, 255", "102, 255, 153", "204, 255, 102", "255, 102, 204"];
	var rand = "";

	<?php $cc = 1; ?>
	<?php foreach ($chart as $x): ?>

	$(document).ready(function() {

		<?php $ch = json_encode($x) ?>

		var labels<?php echo $cc; ?> = <?php echo $ch;?>.map(function(e) {
			return e.tanggal;
		});
		var data1_<?php echo $cc;?> = <?php echo $ch;?>.map(function(e) {
			return e.total;
		});

		var ctxs<?php echo $cc; ?> = document.getElementById("myCharts<?php echo $cc; ?>").getContext('2d');
		var ctx<?php echo $cc; ?> = document.getElementById("myChart<?php echo $cc; ?>").getContext('2d');

		rand = back[Math.floor(Math.random() * back.length)];
		var gradientFills = ctxs<?php echo $cc; ?>.createLinearGradient(0, 0, 0, 290);
		var gradientFill = ctx<?php echo $cc; ?>.createLinearGradient(0, 0, 0, 290);
		gradientFills.addColorStop(0, "rgba(" + rand + ", 1)");
		gradientFills.addColorStop(1, "rgba(" + rand + ", 0.1)");
		gradientFill.addColorStop(0, "rgba(" + rand + ", 1)");
		gradientFill.addColorStop(1, "rgba(" + rand + ", 0.1)");

		var config = {
			type: 'line',
			data: {
				labels: labels<?php echo $cc;?>,
				datasets: [{
					label: 'Total Pendaftar Calon Jawara',
					data: data1_<?php echo $cc;?>,
					pointBackgroundColor: 'white',
					pointBorderWidth: 1,
					backgroundColor: gradientFill,
					borderColor: 'rgba(' + rand + ', 1)',
					lineTension: 0
				}]
			},
			options: {
				scales: {
					yAxes: [{
						ticks: {
							fontColor: "#aaaaaa",
							beginAtZero: true,
							userCallback: function(value, index, values) {
								// Convert the number to a string and splite the string every 3 charaters from the end
								value = value.toString();
								value = value.split(/(?=(?:...)*$)/);
								// Convert the array to a string and format the output
								value = value.join('.');
								return value;
							}
						},
						gridLines: { color: "#444444" }
					}],
					xAxes: [{
						ticks: {
							fontColor: "#aaaaaa",
						},
						gridLines: { color: "#444444" }
					}]
				},
				legend: {
					display: true,
					labels: {
					fontColor: "#aaaaaa"
					}
				},
				responsive: true,
				chartArea: {
					backgroundColor: 'rgba(251, 85, 85, 0.4)'
				}
			}
		};

		var charts<?php echo $cc; ?> = new Chart(ctxs<?php echo $cc; ?>, config);
		var chart<?php echo $cc; ?> = new Chart(ctx<?php echo $cc++; ?>, config);

	});

	<?php endforeach; ?>
</script>

<!-- Score Card -->
<script type="text/javascript">

	var handleVisitorsDonutChart = function() {
		var visitorDonutChartData = [

			<?php foreach ($pendaftar as $x): ?>
				{ 'label': '<?= $x->tanggal ?>', 'value' : <?= $x->total ?>, 'color': COLOR_GREEN },

			<?php endforeach; ?>
		];
		var arcRadius = [
			{ inner: 0.65, outer: 0.93 },
			{ inner: 0.65, outer: 0.93 },
			{ inner: 0.65, outer: 0.93 },
			{ inner: 0.65, outer: 0.93 },
			{ inner: 0.65, outer: 0.93 },
			{ inner: 0.65, outer: 0.93 },
			{ inner: 0.65, outer: 0.93 },
			{ inner: 0.65, outer: 0.93 }
		];

		nv.addGraph(function() {
		var donutChart = nv.models.pieChart()
			.x(function(d) { return d.label })
			.y(function(d) { return d.value })
			.margin({'left': 10,'right':  10,'top': 10,'bottom': 10})
			.showLegend(false)
			.donut(true) 
			.growOnHover(false)
			.arcsRadius(arcRadius)
			.donutRatio(0.5);
			
			donutChart.labelFormat(d3.format(',.0f'));
			
			d3.select('#visitors-donut-chart').append('svg')
				.datum(visitorDonutChartData)
				.transition().duration(3000)
				.call(donutChart);
			
			return donutChart;
		});
	};

	var handleVisitorsVectorMap = function() {
		if ($('#visitorss-map').length !== 0) {
			$('#visitorss-map').vectorMap({
				map: 'world_mill',
				scaleColors: [COLOR_DARK_LIGHTER, COLOR_DARK],
				container: $('#visitorss-map'),
				normalizeFunction: 'linear',
				hoverOpacity: 0.5,
				hoverColor: false,
				zoomOnScroll: false,
				markerStyle: {
					initial: {
						fill: COLOR_DARK,
						stroke: 'transparent',
						r: 3
					}
				},
				regions: [{
					attribute: 'fill'
				}],
				regionStyle: {
					initial: {
						fill: COLOR_DARK_LIGHTER,
						"fill-opacity": 1,
						stroke: 'none',
						"stroke-width": 0.4,
						"stroke-opacity": 1
					},
					hover: {
						"fill-opacity": 0.8
					},
					selected: {
						fill: 'yellow'
					}
				},
				series: {
					regions: [{
						values: {
							ID: COLOR_BLUE
						}
					}]
				},
				// focusOn: {
				// 	// latLng: [47.774099, -52.793427],
				// 	latLng: [-6.200000, 106.816666],
                //     scale: 5
				// },
				latLngToPoint: {
					lat: -6.200000,
					lng: 106.816666
				},
				backgroundColor: 'transparent'
			});
		}
	};

	var DashboardV2 = function () {
		"use strict";
		return {
			//main function
			init: function () {
				handleVisitorsDonutChart();
				handleVisitorsVectorMap();
			}
		};
	}();

	$(document).ready(function() {
		DashboardV2.init();
	});

	setTimeout(function() {
		location.reload();
	}, 1200000);
</script>


