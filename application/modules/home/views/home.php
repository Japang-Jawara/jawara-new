<div id="content" class="content">
	<ol class="breadcrumb float-xl-right">
		<li class="breadcrumb-item"><a href="<?php echo base_url('home');?>">Dashboard</a></li>
	</ol>
	<!-- <h1 class="page-header mb-3">&nbsp;</h1> -->
	<div class="d-sm-flex align-items-center mb-3">
		<a href="#" class="btn btn-inverse mr-2 text-truncate" id="daterange-filter">
			<i class="fa fa-calendar fa-fw text-white-transparent-5 ml-n1"></i> 
			<span><?php echo date("d/m/Y"); ?></span>
		</a>
	</div>

	<div class="row">
		<!-- begin col-6 -->
		<div class="col-xl-6">
			<!-- begin card -->
			<div class="card border-0 bg-dark text-white mb-3 overflow-hidden">
				<!-- begin card-body -->
				<div class="card-body">

					<!-- begin row -->
					<div class="row">
						<!-- begin col-7 -->
						<div class="col-xl-7 col-lg-8">
							<!-- begin title -->
							<div class="mb-3 text-grey">
								<b>TOTAL JAWARA LEADS</b>
								<span class="ml-2">
									<i class="fa fa-info-circle" data-toggle="popover" data-trigger="hover" data-title="Total jawara" data-placement="top" data-content="Total jawara saat ini."></i>
								</span>
							</div>
							<!-- end title -->
							<!-- begin total-sales -->
							<div class="d-flex mb-1">
								<h2 class="mb-0"><span data-animation="number" data-value="<?= $totalLeads[0]->totalLeads; ?>"><?= $totalLeads[0]->totalLeads; ?></span></h2>
								<div class="ml-auto mt-n1 mb-n1"><div id="total-sales-sparkline"></div></div>
							</div>
							<!-- end total-sales -->
							<!-- begin percentage -->
							<div class="mb-3 text-grey">
								<i class="fa fa-caret-up"></i> + <span data-animation="number" data-value="<?= $leadsHariIni[0]->leadsHariIni; ?>"><?= $leadsHariIni[0]->leadsHariIni; ?></span> Pendaftar Hari ini
							</div>
							<!-- end percentage -->
							<hr class="bg-white-transparent-2" />
							<h2>JaPang</h2>
						</div>
						<!-- end col-7 -->
						<!-- begin col-5 -->
						<div class="col-xl-5 col-lg-4 align-items-center d-flex justify-content-center">
							<img src="<?= base_url('assets/img/svg/img-1.svg'); ?>" height="150px" class="d-none d-lg-block" />
						</div>
						<!-- end col-5 -->
					</div>
					<!-- end row -->
					
				</div>
				<!-- end card-body -->
			</div>
			<!-- end card -->
		</div>
		<!-- end col-6 -->
		<div class="col-xl-3 col-md-6">
			<div class="widget widget-stats bg-blue">
				<div class="stats-icon"><i class="fa fa-users"></i></div>
				<div class="stats-info">
					<h4>NOT YET KYC</h4>
					<p><?= $notyetKYC[0]->notyetKYC; ?> Jawara</p>
					<div class="mb-3 text-white">
						<i class="fa fa-caret-up"></i> - <span data-animation="number" data-value="<?= $doneKYCHari[0]->doneKYCHari; ?>"><?= $doneKYCHari[0]->doneKYCHari; ?></span> Belum KYC Hari ini
					</div>
				</div>
				<div class="stats-link">
					<a href="<?= base_url('index.php/JAWARA'); ?>">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
				</div>
			</div>
		</div>

		<div class="col-xl-3 col-md-6">
			<div class="widget widget-stats bg-info">
				<div class="stats-icon"><i class="fa fa-desktop"></i></div>
				<div class="stats-info">
					<h4>DONE KYC</h4>
					<p><?= $doneKYC[0]->doneKYC; ?> Jawara</p>
					<div class="mb-3 text-white">
						<i class="fa fa-caret-up"></i> + <span data-animation="number" data-value="<?= $doneKYCHari[0]->doneKYCHari; ?>"><?= $doneKYCHari[0]->doneKYCHari; ?></span> Done KYC Hari ini
					</div>	
				</div>
				<div class="stats-link">
					<a href="<?= base_url('index.php/kyc'); ?>">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
				</div>
			</div>
		</div>
	</div>
	<!-- end row -->

	<div class="row">
		<div class="col-xl-4">
			<div class="widget-chart with-sidebar inverse-mode">
				<div class="widget-chart-content bg-dark">
					<h4 class="chart-title" >
						Digital Inspection
						<small>Jawara</small>
					</h4>
					<div class="widget-data update-label">
						<span class="badge badge-pill label-date" style="font-size: 9px;" data-bgcolor="#e7ebf5" data-color="#ea7510"><?= date('Y-m-d') ?></span>
					</div>
					<canvas id="chart-line" width="100" height="100"></canvas>
				</div>
			</div>
		</div>

		<div class="col-xl-8">
			<div class="widget-chart with-sidebar inverse-mode">
				<div class="widget-chart-content bg-white">
					<h4 class="chart-title" style="color:black">
						Score Card/On Ground
						<small>Jawara</small>
					</h4>
					<div class="widget-data update-label">
						<span class="badge badge-pill label-date" style="font-size: 9px;" data-bgcolor="#e7ebf5" data-color="#ea7510"><?= date('Y-m-d') ?></span>
					</div>
					<canvas id="myChart" width="100%"></canvas>
				</div>
				<div class="widget-chart-sidebar bg-dark-darker">
					<div class="chart-number">
						<?= $scoreCard->TOTAL ?>
						<small>Total Score Card/On Ground</small>
					</div>
					<div class="flex-grow-1 d-flex align-items-center">
						<div id="visitors-donut-chart" class="nvd3-inverse-mode" style="height: 180px"></div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>

<script src="<?php echo base_url().'assets/js/jquery.min.js'?>"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.0/chart.min.js"></script>

<!-- Digital Inspection -->
<script>
    $(document).ready(function() {
        var ctx = $("#chart-line");
        var myLineChart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: [
					'A+',
					'A',
					'B+',
					'B',
					'C',
					'C-',
					'D',
					'E'
				],
                datasets: [{
                    data: [
						<?= $digitalInspection->APLUS ?>,
						<?= $digitalInspection->A ?>,
						<?= $digitalInspection->BPLUS ?>,
						<?= $digitalInspection->B ?>,
						<?= $digitalInspection->C ?>,
						<?= $digitalInspection->CMIN ?>,
						<?= $digitalInspection->D ?>,
						<?= $digitalInspection->E ?>
					],
                    backgroundColor: [
						"rgba(0, 100, 255, 0.5)",
						'rgba(102, 16, 242, 1)',
						'rgba(111, 66, 193, 1)',
						'rgba(232, 62, 140, 1)',
						'rgba(220, 53, 69, 1)',
						'rgba(253, 126, 20, 1)',
						'rgba(255, 193, 7, 1)',
						'rgba(253, 100, 0, 1)'
					],
					borderColor: [
						"rgba(0, 100, 255, 0.5)",
						'rgba(102, 16, 242, 1)',
						'rgba(111, 66, 193, 1)',
						'rgba(232, 62, 140, 1)',
						'rgba(220, 53, 69, 1)',
						'rgba(253, 126, 20, 1)',
						'rgba(255, 193, 7, 1)',
						'rgba(253, 100, 0, 1)'
					],
					borderWidth: 4,
				}]
            },
            options: {
                title: {
                    display: true,
                    text: 'Weather'
                }
            }
        });
    });
</script>

<!-- Score Card -->
<script type="text/javascript">
	const ctx = document.getElementById('myChart').getContext('2d');
	const myChart = new Chart(ctx, {
		type: 'line',
		data: {
			labels: [
				'A+',
				'A',
				'B+',
				'B',
				'C',
				'C-',
				'D',
				'E'
			],
			datasets: [{
				label: '<?= $scoreCard->TOTAL ?> SCORE CARD/ON GROUND',
				data: [
					<?= $scoreCard->APLUS ?>,
					<?= $scoreCard->A ?>,
					<?= $scoreCard->BPLUS ?>,
					<?= $scoreCard->B ?>,
					<?= $scoreCard->C ?>,
					<?= $scoreCard->CMIN ?>,
					<?= $scoreCard->D ?>,
					<?= $scoreCard->E ?>
				],
				backgroundColor: [
					"rgba(0, 100, 255, 0.5)",
					'rgba(102, 16, 242, 1)',
					'rgba(111, 66, 193, 1)',
					'rgba(232, 62, 140, 1)',
					'rgba(220, 53, 69, 1)',
					'rgba(253, 126, 20, 1)',
					'rgba(255, 193, 7, 1)',
					'rgba(253, 100, 0, 1)'
				],
				borderColor: [
					"rgba(0, 100, 255, 0.5)",
					'rgba(102, 16, 242, 1)',
					'rgba(111, 66, 193, 1)',
					'rgba(232, 62, 140, 1)',
					'rgba(220, 53, 69, 1)',
					'rgba(253, 126, 20, 1)',
					'rgba(255, 193, 7, 1)',
					'rgba(253, 100, 0, 1)'
				],
				borderWidth: 4,
				grid: {
					show: false,
				}
			}]
		},
		responsive: true,
		options: {
			scales: {
				y: {
					display: true,
					title: {
						display: true,
						text: 'Jumlah Score Card / On ground'
					}
				}
			},
			tooltip: {
				shared: true,
				valueSuffix: ' units'
			},
		}
	});

	var handleVisitorsDonutChart = function() {
		var visitorDonutChartData = [
			{ 'label': 'A+', 'value' : <?= $scoreCard->APLUS ?>, 'color': COLOR_BLUE },
			{ 'label': 'A', 'value' : <?= $scoreCard->A ?>, 'color': COLOR_GREEN }, 
			{ 'label': 'B+', 'value' : <?= $scoreCard->BPLUS ?>, 'color': COLOR_YELLOW }, 
			{ 'label': 'B', 'value' : <?= $scoreCard->B ?>, 'color': COLOR_ORANGE }, 
			{ 'label': 'C', 'value' : <?= $scoreCard->C ?>, 'color': COLOR_PURPLE }, 
			{ 'label': 'C-', 'value' : <?= $scoreCard->CMIN ?>, 'color': COLOR_PINK }, 
			{ 'label': 'D', 'value' : <?= $scoreCard->D ?>, 'color': COLOR_GREY }, 
			{ 'label': 'E', 'value' : <?= $scoreCard->E ?>, 'color': COLOR_RED }
		];
		var arcRadius = [
			{ inner: 0.65, outer: 0.93 },
			{ inner: 0.65, outer: 0.93 },
			{ inner: 0.65, outer: 0.93 },
			{ inner: 0.65, outer: 0.93 },
			{ inner: 0.65, outer: 0.93 },
			{ inner: 0.65, outer: 0.93 },
			{ inner: 0.65, outer: 0.93 },
			{ inner: 0.65, outer: 0.93 }
		];

		nv.addGraph(function() {
		var donutChart = nv.models.pieChart()
			.x(function(d) { return d.label })
			.y(function(d) { return d.value })
			.margin({'left': 10,'right':  10,'top': 10,'bottom': 10})
			.showLegend(false)
			.donut(true) 
			.growOnHover(false)
			.arcsRadius(arcRadius)
			.donutRatio(0.5);
			
			donutChart.labelFormat(d3.format(',.0f'));
			
			d3.select('#visitors-donut-chart').append('svg')
				.datum(visitorDonutChartData)
				.transition().duration(3000)
				.call(donutChart);
			
			return donutChart;
		});
	};

	var DashboardV2 = function () {
		"use strict";
		return {
			//main function
			init: function () {
				handleVisitorsDonutChart();
			}
		};
	}();

	$(document).ready(function() {
		DashboardV2.init();
	});
</script>


