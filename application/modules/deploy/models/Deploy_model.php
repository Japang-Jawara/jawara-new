<?php
class Deploy_model extends CI_Model{


    public function getOpenDeploy(){
        return $this->db->select('*')->from('jawara_leads')->where('status_confirm_dis IS NOT NULL')->where('status_deploy_dis', NULL)->get()->result();
    }

    public function getDoneDeploy(){
        return $this->db->select('*')->from('jawara_leads')->where('created_at_dep IS NOT NULL')->get()->result();
    }

    public function getDeploy(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'SDEPLOY')->get()->result();
    }

    public function createDeploy($data, $data2, $id){

        $result = $this->db->where('id', $id)->update('jawara_leads', $data);

        if($result){
            $this->db->where('id', $id)->update('jawara_leads', $data2);
            $this->session->set_flashdata('success','Deploy Telah Berhasil Diinput Ke Sistem');
            return TRUE;
        }else{
            $this->session->set_flashdata('success','Deploy Gagal Diinput Ke Sistem, Silahkan Coba Lagi');
            return FALSE;
        }
    }

}
?>