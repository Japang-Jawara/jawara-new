<?php
defined('BASEPATH') OR exit('No direct script access allowed ');

class User extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //load model
        $this->load->model('User_model', 'modelUser');
    }

    public function index()
	{
        if($this->session->userdata('is_login') == TRUE || $this->session->userdata('is_p2p') == TRUE){

            $x['data'] = $this->modelUser->getUser()[0];

            $this->load->view("include/head");
            $this->load->view("include/top-header");
            $this->load->view('users', $x);
            $this->load->view("include/sidebar");
            $this->load->view("include/panel");
            $this->load->view("include/footer");
            $this->load->view("include/alert");
        }else{
            $this->session->set_flashdata('success', 'Upsss!!!, Login dulu ya.');
            redirect('index.php/login');
        }
    }

    function upload($namee) {
        foreach($_FILES as $name => $fileInfo)
        {
            $filename=$_FILES[$name]['name'];
            $tmpname=$_FILES[$name]['tmp_name'];
            $exp=explode('.', $filename);
            $ext=end($exp);
            $newname= 'profile_'.$namee.'_'.time().".".$ext; 
            $config['upload_path'] = './assets/img/user/';
            $config['upload_url'] =  base_url().'assets/img/user/';
            $config['allowed_types'] = "gif|jpg|jpeg|png";
            $config['max_size'] = '2000000';
            $config['file_name'] = $newname;
            $this->load->library('upload', $config);
            move_uploaded_file($tmpname,"assets/img/user/".$newname);
            return $newname;
        }
    }

    public function update(){

        $profile_pic = 'user-12.jpg';
        $namee = $this->input->post('first_name', true);

        if($this->input->post('fileOld')) {  
            $newname = $this->input->post('fileOld');
            $profile_pic =$newname;
        } else {
            $profile_pic ='user-12.jpg';
        }

        foreach($_FILES as $name => $fileInfo)
        { 
            if(!empty($_FILES[$name]['name'])){
                $newname=$this->upload($namee); 
                $data[$name]=$newname;
                $profile_pic =$newname;
                $input_data['profile_picture'] = base_url('assets/img/user/').$profile_pic;
            } else {  
                if($this->input->post('fileOld')) {  
                    $newname = $this->input->post('fileOld');
                    $data[$name]=$newname;
                    $profile_pic =$newname;

                    $input_data['profile_picture'] = $profile_pic;
                } else {
                    $data[$name]='';
                    $profile_pic ='user-12.jpg';
                    $input_data['profile_picture'] = base_url('assets/img/user/').$profile_pic;
                } 
            } 
        }

        $password = $this->input->post('password', true);

        $input_data['first_name'] = $this->input->post('first_name', true); 			
        $input_data['last_name'] = $this->input->post('last_name', true);

        if($password != NULL){
            $input_data['password'] = md5($this->input->post('password', true));
        }

        $result = $this->modelUser->update($input_data);

        if (!$result) { 							
            $this->session->set_flashdata('success', 'Gagal mengubah profil.'); 				
            redirect('index.php/user'); 			
        } else { 								
            $this->session->set_flashdata('success', 'Profile Sudah Berhasil Diubah, Ya.');
            $this->session->set_userdata('first_name',$input_data['first_name']);
            $this->session->set_userdata('last_name',$input_data['last_name']);
            $this->session->set_userdata('profile_picture',$input_data['profile_picture']);				
            redirect('index.php/user'); 			
        }

    }
}
?>