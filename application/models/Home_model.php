<?php
class Home_model extends CI_Model{

    public function totalLeads(){
        return $this->db->query("SELECT COUNT(*) totalLeads FROM jawara_leads")->result();
    }

    public function leadsHariIni(){
        $date = date('Y-m-d');
        return $this->db->query("SELECT COUNT(*) as leadsHariIni FROM jawara_leads WHERE created_at LIKE '%$date%'")->result();
    }

    public function notyetKYC(){
        return $this->db->query("SELECT COUNT(*) notyetKYC FROM jawara_leads WHERE status_kyc IS NULL")->result();
    }

    public function doneKYC(){
        return $this->db->query("SELECT COUNT(*) doneKYC FROM jawara_leads WHERE status_kyc = 1")->result();
    }

    public function doneKYCHari(){
        $date = date('Y-m-d');
        return $this->db->query("SELECT COUNT(*) doneKYCHari FROM jawara_leads WHERE status_kyc = 1 AND updated_at LIKE '%$date%'")->result();
    }
    
    public function lolosSurvey(){
        $inspection = $this->db->select('count(*) as inspect')->from('jawara_leads')->where('status_jawara_di',1)->get()->result()[0]->inspect;
        $score = $this->db->select('count(*) as score')->from('jawara_leads')->where('status_score_sc',1)->get()->result()[0]->score;

        return $inspection+$score;
    }

    public function lolosSurveyHari(){
        $date = date('Y-m-d');
        $inspection = $this->db->select('count(*) as inspect')->from('jawara_leads')
        ->where('status_jawara_di',1)
        ->like('created_at_di',$date)
        ->get()->result()[0]->inspect;

        $score = $this->db->select('count(*) as score')->from('jawara_leads')
        ->where('status_score_sc',1)
        ->like('created_at_sc',$date)
        ->get()->result()[0]->score;

        return $inspection+$score;
    }

    public function totalDisbursement(){
        return $this->db->select('count(*) as totalDisbursement')->from('jawara_leads')->where('status_disbursement', 1)->get()->result()[0]->totalDisbursement;
    }

    public function totalDisbursementHari(){
        $date = date('Y-m-d');
        return $this->db->select('count(*) as totalDisbursement')->from('jawara_leads')->where('status_disbursement', 1)
        ->like('created_at',$date)
        ->get()->result()[0]->totalDisbursement;
    }

    public function totalDeploy(){
        return $this->db->select('count(*) as totalDeploy')->from('jawara_leads')->where('created_at_dep IS NOT NULL')->get()->result()[0]->totalDeploy;
    }

    public function totalDeployHari(){
        $date = date('Y-m-d');
        return $this->db->select('count(*) as totalDeploy')->from('jawara_leads')->where('created_at_dep IS NOT NULL')
        ->like('created_at',$date)
        ->get()->result()[0]->totalDeploy;
    }

    public function totalperProvinsi(){
        return $this->db->select('upper(provinsi) as provinsi, count(*) as total')->from('jawara_leads')
        ->where('provinsi', 'DKI JAKARTA')
        ->or_where('provinsi', 'JAWA BARAT')
        ->or_where('provinsi', 'JAWA TIMUR')
        ->group_by('provinsi')->get()->result();
    }

    public function getTotal(){
        return $this->db->query("SELECT total, tanggal
            FROM (SELECT count(*) as total, DATE_FORMAT((`created_at`), '%d-%m-%Y') AS tanggal
            FROM jawara_leads
            GROUP BY DATE_FORMAT((`created_at`), '%d-%m-%Y')
            ORDER BY created_at DESC
            LIMIT 7) a
            ORDER BY tanggal ASC");

        // $this->db->select("count(*) as total, DATE_FORMAT((`created_at`), '%d-%m-%Y') AS tanggal ");
		// $this->db->from('jawara_leads');
		// $this->db->group_by("DATE_FORMAT((`created_at`), '%d-%m-%Y')");
        // $this->db->order_by('created_at', 'DESC');
        // $this->db->limit(7);
		// $result = $this->db->get();

		// return $result;
    }

    public function maps(){
        return $this->db->query("SELECT gmaps_di FROM jawara_leads WHERE gmaps_di IS NOT NULL AND gmaps_di != 0")->result();
    }

}
?>